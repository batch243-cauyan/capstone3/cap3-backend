const express = require('express');
const router = express.Router();

const userController = require('../controllers/userController');
const auth = require('../auth');


// endpoint: get All user
router.get('/', auth.verify, userController.getAllUser);

router.get('/viewprofile', auth.verify, userController.profileDetails);

// register a user
router.post('/register',userController.isEmail, userController.checkEmailExists, userController.registerUser);


// login
router.post('/login', userController.loginUser);

router.get('/mycart', auth.verify, userController.viewCart);

// toggle admin Role
router.patch('/adminize/:userId', auth.verify,  userController.updateRole)



module.exports = router;
