const mongoose = require('mongoose')

// const Schema = mongoose.Schema

const productSchema = new mongoose.Schema({
    prodName: {
        type:String,
        required:true
    },
    price:{
        type:Number,
        required:true
    },
    //category?
    description:{
        type:String,
        default: "description",
        maxLength:300
    },
    category:{
        type:String,
        default:"others"
    },
    specs:{
        dimensions:{
            type:String,
            maxLength: 30,
        },
        colors: [{
            type:String,
            maxLength:30
        }],
        size: [{
            type:String,
            maxLength:10
        }]
    },
    stocks:{
        type:Number,
        required:true
    },
    isAvailable:{
        type:Boolean,
        default:true
    },
    isOnsale:{
        type:Boolean,
        default:false
    },
    discountedPrice:{
        type: Number
    }
})

productSchema.set('timestamps', true);
module.exports = mongoose.model("Product", productSchema);