require('dotenv').config();
const Product = require('../models/product');
const router = require('../routes/orderRoutes');
const auth = require('../auth');
const Order = require('../models/orders')
const User = require('../models/user');
const { default: mongoose } = require('mongoose');


// Create an order
module.exports.placeOrder = async (req,res)=>{

    let userData = auth.decode(req.headers.authorization)

    let order = new Order({
        userId: userData.id,
        productsOrdered:[],
        totalAmount: 0
    })


    if (!userData.isAdmin){
        
        await User.findById(userData.id).then(result=>{
         
            if (result.shoppingCart.length > 0){
                result.shoppingCart.forEach((item)=>{
                    order.totalAmount += item.subTotal;
                    order.productsOrdered.push(item);
                })
                
                result.updateOne({$set: {shoppingCart: []}})
                .then(()=>console.log(`emptied cart`))
                .catch(error=>res.send(error))
                
                //save Order
                order.save()
                .then(savedOrder=> {
                   
                    return res.status(201).json({
                        message: `Placed the following order:`,
                        Order: savedOrder,
                        orderSuccess:true
                    })
                })
                .catch(error =>{
                    return res.send(error);
                })
            }
            else{
                res.status(400).json({error:`Cart is empty.`});
            }
        
        }).catch(error =>{
            return res.send (error);
        })

        /*
        savedOrder.populate('productOrdered.productId', ['prodName' ,'price','discountedPrice'])).then((result)=>
        */
    }
    else{
        res.send(`Error: Invalid User, you don't have access to this feature.`);
    }

}

//FUNCTIONAL
module.exports.singleOrder = async (req,res)=>{
    let userData = auth.decode(req.headers.authorization);
    let prodId = req.body.productId
    let qty = req.body.quantity

   
    console.log(`This is reqbody`,req.body);

    let order = new Order({
        userId: userData.id,
        productsOrdered: [{
            productId : prodId,
            quantity: qty,
            subTotal: 0
        }],
        totalAmount: 0
    })

    if (!userData.isAdmin ) {
        //Write Codes Here
        if (prodId!==null){
            await Product.findById(prodId).then(result=>{
               
                if (!result.isOnsale){
                     //set order subtotal and total amount
                    order.productsOrdered[0].subTotal = result.price * qty;
                    order.totalAmount= result.price * qty;
                 }
                else{
                    order.productsOrdered[0].subTotal = result.discountedPrice * qty;
                    order.totalAmount = result.discountedPrice * qty;
                 }

                console.log(`this is order`,order)

            }).then(()=>{
                console.log(`Before save `,order);
                order.save()
                return res.status(201).json({message:`Successfully placed an Order`, Order:order})
                
            }).catch(error =>
                {res.send(error)})

        }
        else {  
            return res.send(`Enter Valid Product ID`); 
        }
    }
    else{
        return res.send(`You don't have access to this feature. Please Login`)
    }
}

module.exports.approveSingleOrder = async(req,res)=>{
    let userData =  auth.decode(req.headers.authorization);
    let session  = null;
    let orderId = req.body.orderId;
    // let update = {
    //     status: "approved"
    // }
    let prodId;
    let orderQty;
    let usertransaction = {
        status: "completed",
        orderId: orderId
        
    }

    let clientId;


    if (userData.isAdmin){

        await Order.findById(orderId)
        .then((result)=>{
            // console.log(`This is result`,result)
            if (result.status !== "approved"){

                orderQty = result.productsOrdered[0].quantity;
                // console.log(`This is order Qty`,orderQty);
                prodId = result.productsOrdered[0].productId;
                result.status = "approved";
                clientId = result.userId;
                result.save().then(()=>{

                    return  Product.findOne({_id: prodId})
                    .then(product =>{
                        console.log(product.stocks)
                        console.log("before decrement",product.stocks);
                        product.stocks -= orderQty;
             
                        console.log("after decrement",product);
                        product.save()
                        console.log('success')
                        return res.status(200).json({message:`Transaction was successful`, document:product})
                    })
                }).then(()=>{
                    return User.findById(clientId).then((user)=>{
                        user.userTransactions.push(usertransaction);
                        user.save();
                        console.log(`Updated user transactions ${user.userTransactions}`);
                    })
                })
                    
            }
            
            else{
                 return res.send(`Order was already approved.`)
            }
            
         
        }).catch(error => {
            return res.send(error)})
    }   
    else{
        return res.send(`You are unathorized for this feature.`)
    }



}


module.exports.getSingleOrder = async (req,res)=>{
    let userData = auth.decode(req.headers.authorization);
    let orderId = req.params.orderId;

    if (!userData.isAdmin) {

    await Order.findById(orderId).populate([{

        path: 'productsOrdered.productId',
        model: 'Product',
        select: 'prodName price description category'
        }
        ])
          .then(result =>{
            res.status(200).json({order: result});
          })
          .catch(error => res.send(error))
    }
    else{
        res.status(403).json({error:`Unauthorized Access` });
    }      
}






module.exports.getAllOrders = async (req,res)=>{
    let userData = auth.decode(req.headers.authorization);
    let filter = {};
    filter = req.body.filter;
    

    if (userData.isAdmin) {

    await Order.find().sort({status:1, createdAt:1 })
          .then(result =>{
            return res.status(200).json({message:`Retrieved Orders`, totalOrders:result.length , orders: result});
          })
          .catch(error => {res.send(error)})
    }
    else{
        res.status(403).json(`Unauthorized Access`);
    }      
}


module.exports.filterOrders = async (req,res)=>{
    let userData = auth.decode(req.headers.authorization);
    let qstatus = (req.query.status);
    //desc or asc
    let qsort = parseInt(req.query.sort);
    // let qcategory = req.query.category;
    console.log(req.query);
    if (userData.isAdmin && (qstatus === "approved"|| qstatus==="pending")) {

    await Order.find({status:qstatus})
          .then(result =>{
            res.status(200).json({message:`Retrieved Orders`, totalOrders:result.length , orders: result});
          })
          .catch(error => {res.send(error)})
    }
    else if((qstatus !== "approved" || qstatus!=="pending")){
        res.status(403).json(`Enter a valid query.`);
    }
    else{
        res.status(403).json(`Unauthorized access. You must be an admin.`);
    }      
}



module.exports.approveOrders = async (req,res)=>{
    let userData = auth.decode(req.headers.authorization);
    let orderId = req.query.orderid;
    // userId from order
    let userId ;
    let orderTotalAmount;
    let qstatus = req.query.status;
    // IF UPDATEDPRODUCTSTOCKS.LENGTH === ORDERSWITHSTOCK.LENGTH THEN PROCEED WITH TRANSACTION
    let orderswithstock = []
    // IF THERE ARE ORDERS WITHOUT STOCK PROMPT THE ADMIN ABOUT IT BEFORE PROCEEDING 
    let orderwithoutstock = []
    // contains an array orderObject: {productId, quantity, subTotal}
    let orders = []
    // variable contains an array of productIds of the ordered Products which will be used for finding
    let orderproductIds = []
    let updatedProductStocks= []
    // let updateOrder = {
    //     status: qstatus
    // }

    let usertransaction = {
        status: "",
        orderId: orderId,
        // totalAmount: 0
    }

    // const session =  await mongoose.startSession();

    /*
        1. Check if ordered products quantity is less than stocks available.
        1. Upon entering of an orderId the status of the order must be approved.
        2. Once approved stocks of the products inlcuded in the orders must be decremented respectively
        3. Once transaction is complete order will be pushed to user's transaction history.

        must add - approve the order only after the order quantity has been successfully reduced from the available stokcs.
         IF UPDATEDPRODUCTSTOCKS.LENGTH === ORDERSWITHSTOCK.LENGTH THEN PROCEED WITH TRANSACTION
         IF THERE ARE ORDERS WITHOUT STOCK PROMPT THE ADMIN ABOUT IT BEFORE PROCEEDING with the transaction.
    */


    if (userData.isAdmin){
        
        // session.startTransaction()
        try{
            // const opts = {session, new:true}
            // , opts
  
            console.log(`StARTED`)
            await Order.findById(orderId)
            .then(async result=>{
                // console.log('Current Status',result.status)
                userId = result.userId;
                orderTotalAmount = result.totalAmount;
                if (qstatus === "approved" && result.status === "pending"){

                        //looping through orders to get properties
                        result.productsOrdered.forEach( product => {
                            orders.push(product);
                            orderproductIds.push(product.productId)
                            console.log(product.productId);
                        });//end of foreach loop
                           
                            result.status = "approved";
                            result.save();
                        //  console.log(`These are the orders:`,orders);
                        
                    try {
                        //get number of stocks available
                        await Product.find().where('_id').in(orderproductIds).then(products=>{
                            // console.log(`THESE are the products in find IN:`,products)

                            products.forEach( product2 =>{
                             
                                for (let i=0; i<products.length; i++){
                                    // console.log(`This is products length:${products.length} This is products2id${product2._id} and this is ordersId${orders[i].productId}`)
                                    
                                    if(product2._id.equals(orders[i].productId)){
                                        // console.log(`IDs matched`)
                                            //checks if product has enough stocks for the order
                                            if (product2.stocks > orders[i].quantity){
                                                product2.stocks -= orders[i].quantity
                                                // console.log(`Stocks remaining ${product2.stocks}`)
                                                
                                                // populate product with stock and updatedProductStocks = used for updating multiple documents
                                                orderswithstock.push(product2);
                                                updatedProductStocks.push(product2);
                                                break;
                                            
                                            }
                                            else{
                                                orderwithoutstock.push(product2)
                                                break;
                                            
                                            }
                                        }
                                 }
                            })//end of foreach product2
                            
                        }).then( ()=>{

                            // console.log(`UpdatedStocks: `+updatedProductStocks);
                            console.log(`Order without enough stocks:`,orderwithoutstock);
                            console.log(`Order with enough stocks:`,orderswithstock);
        
                            let bulkUpdateCallback = function(err, r){
                                console.log(`Matched count `,r.matchedCount);
                                console.log(`Modified count`,r.modifiedCount);
                            }
                            // Initialise the bulk operations array
                            let bulkOps = updatedProductStocks.map(function (updatedproduct) { 
                                return { 
                                    "updateOne": { 
                                        "filter": { "_id": updatedproduct._id } ,              
                                        "update": { "$set": {stocks: updatedproduct.stocks} } 
                                    }         
                                }    
                            });
                            
                            Product.bulkWrite(bulkOps, { "ordered": true, w: 1 }, bulkUpdateCallback)  
                            
                        }).then(()=>{
                            console.log(`HEEY This is the user ID: ${userId}`)
            
                            User.findById(userId).then(retrievedUser=>{
                                usertransaction.status = "complete";
                                // usertransaction.totalAmount = orderTotalAmount;
                                retrievedUser.userTransactions.push(usertransaction);
                                
                                return retrievedUser.save();
                            }).then((result)=>{
                                console.log(`Updated User transaction Histoty`)
                                console.log(`These are my transacitons`,result.userTransactions)
                                return res.status(200).json({message:"Transaction Complete.", details:result});
                            })
                        })
                        .catch((error)=>{
                            return res.send (error)
                        })

                    } //end of inner try block
                        catch (error) {
                        return res.send(error);
                    }       
                }//end of if status approved validation

                else{
                    return res.status(200).json({error:`Order was invalid.`, orderFailed:true })
                }
                
            })

           
        }// end of first try block
        catch(error){
            // await session.abortTransaction();
            // session.endSession();
            return res.status(400).json({message: error});
        }

    
        
     }
     else{
        return res.send(`You don't have access to this feature.`)
     }

}


