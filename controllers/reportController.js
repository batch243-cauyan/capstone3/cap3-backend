const Product = require('../models/product');
const router = require('../routes/productRoutes');
const auth = require('../auth');
const User = require('../models/user');




// THIS FUNCTION IS NOT YET AVAILABLE
let monthlySales = {
    January: 50000,
    February: 30400,
    March: 75000,
    April: 100000,
    May: 80000,
    June: 60000,

    July: 55000,
    August: 90000,
    September: 100000,
    October: 76000,
    November: 35000,
    December: 123000
}

let sales = {
    firstQtr: {
    January: 50000,
    February: 30400,
    March: 75000}
    ,
    secondQtr: {April: 100000,
    May: 80000,
    June: 60000},
    thirdQtr:{
    July: 55000,
    August: 90000,
    September: 100000
    },
    fourthQtr:{
    October: 76000,
    November: 35000,
    December: 123000
    }
}


// function getSales(startMonth, endMonth){
//     let start = sales.firstQtr[startMonth];
//     let end = sales.firstQtr[endMonth];
//     let total = end - start;
//     return total;
// }

// console.log(getSales("January", "March"));


function monthlySalesReport(startMonth, endMonth) {
    let startMonthIndex = Object.keys(monthlySales).indexOf(startMonth);
    let endMonthIndex = Object.keys(monthlySales).indexOf(endMonth);
    let result = {};
    for (let i = startMonthIndex; i <= endMonthIndex; i++) {
        result[Object.keys(monthlySales)[i]] = Object.values(monthlySales)[i];
    }
    return result;
}

// console.log(monthlySalesReport("January", "March"));


module.exports.salesReport = async (req,res)=>{
    
    function displayQuarter(quarter){
        for(let key in monthlySales[quarter]){
            console.log(key, monthlySales[quarter][key]);
        }
    }
    
    displayQuarter("firstQtr");
    
    //make a function that accepts a month argument and loops through monthlySales and displays the keys and values for that month only
    
    function displayMonth(month){
        for(let key in monthlySales){
            if(monthlySales[key][month]){
                console.log(monthlySales[key][month]);
            }
        }
    }
    
    displayMonth("January");
    
    //make a function that accepts a year argument and loops through monthlySales and displays the keys and values for that year only
    
    function displayYear(year){
        for(let key in monthlySales){
            for(let key2 in monthlySales[key]){
                if(monthlySales[key][key2] === year){
                    console.log(monthlySales[key][key2]);
                }
            }
        }
    }
    
    displayYear(50000);



    console.log(req.query)
    return res.status(200).send(`Sales Report for the period: ${req.query.startDate} to ${req.query.endDate}`)}
